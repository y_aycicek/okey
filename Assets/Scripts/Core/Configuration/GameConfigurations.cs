﻿using System;


namespace Okey.Core.Configuration
{
    class GameConfigurations
    {
        public int colorCount;
        public int numberCount;
        public int dublicationCount;
        public Boolean oneAfterLast;
        public Boolean wildTileEnabled;
        public int tileCount;

        public GameConfigurations()
        {
            colorCount = 4;
            numberCount = 13;
            dublicationCount = 2;
            oneAfterLast = true;
            wildTileEnabled = true;
            tileCount = 14;
        }
    }
}
