﻿

namespace Okey.Core.Configuration
{
    class Conf
    {
        private static Conf conf;

        //singleton pattern thread safe lock. but not required actually.
        private static object unneccessaryLock = new object();

        public SystemConfigurations systemConf;
        public GameConfigurations gameConf;

        private Conf()
        {
            systemConf = new SystemConfigurations();
            gameConf = new GameConfigurations();
        }


        // property set protection for gD
        internal static Conf configuration
        {
            get
            {
                // singleton 
                if (conf == null)
                {
                    lock (unneccessaryLock)
                    {
                        if (conf == null)
                        {
                            conf = new Conf();
                        }
                    }
                }
                return conf;
            }
        }
    }
}
