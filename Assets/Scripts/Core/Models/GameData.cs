﻿using System;
using System.Collections.Generic;


namespace Okey.Core.Models
{
    class GameData
    {
        //singleton Game Data object.
        private static GameData gD;

        //singleton pattern thread safe lock. but not required actually.
        private static object unneccessaryLock = new object();

        public List<TileElement> unusedTiles;

        public List<TileElement> playerTiles;

        public TileElement wildTileElement;

        public int remainingTileCount;


        public List<TileElement> availableTiles = new List<TileElement>();

        public List<TileElement> unpairedTiles = new List<TileElement>();

        public List<List<TileElement>> groups = new List<List<TileElement>>();

        private GameData()
        {
            unusedTiles = new List<TileElement>();
            playerTiles = new List<TileElement>();
        }

        // property set protection for gD
        internal static GameData GD
        {
            get
            {
                // singleton 
                if (gD == null)
                {
                    lock (unneccessaryLock)
                    {
                        if (gD == null)
                        {
                            gD = new GameData();
                        }
                    }
                }
                return gD;
            }
        }
    }
}
