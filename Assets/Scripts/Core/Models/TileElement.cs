﻿using System;

namespace Okey.Core.Models
{
    class TileElement
    {
        //tile index number 
        public int index;

        //tile color
        public TileColor color;

        //tile number written on
        public int number;

        //is this wild tile which is replacement for any tile
        public Boolean isWildTile;

        //is this replacment tile 
        public Boolean isReplacementTile;

        public int oldIndex = 0;

        public override string ToString()
        {
            return color.ToString() + "_" + number;
        }

        
        public string RegexKey()
        {
            return isWildTile ? "OK" : ((char)(color + 65)) + number.ToString("D2");
        }
    }
}
