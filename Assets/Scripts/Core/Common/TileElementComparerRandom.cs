﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Okey.Core.Common;
using Okey.Core.Models;

namespace Okey.Core.Common
{
    class TileElementComparerRandom : IComparer<TileElement>
    {
        public int Compare(TileElement x, TileElement y)
        {
            return RNG.Next(-1, 1);            
        }
    }
}
