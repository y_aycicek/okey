﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Okey.Core.Common
{
    class RNG
    {
        private static Random random = new Random();

        public static void SetSeed(int seed)
        {
            random = new Random(seed);
        }

        public static int Next()
        {
            return random.Next();
        }
        public static int Next(int maxValue)
        {
            return random.Next(maxValue);
        }
        public static int Next(int minValue, int maxValue)
        {
            return random.Next(minValue, maxValue);
        }
    }
}
