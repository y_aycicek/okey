﻿using Okey.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Okey.Core.Common
{
    class TileElementComparerNumber : IComparer<TileElement>
    {
        public int Compare(TileElement x, TileElement y)
        {
            if (x.number > y.number)
            {
                return 1;
            }
            else if (x.number == y.number)
            {
                if (x.color > y.color)
                {
                    return 1;
                }
                else if (x.color == y.color)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }
    }
}
