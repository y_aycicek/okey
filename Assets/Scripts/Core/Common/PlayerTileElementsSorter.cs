﻿using Okey.Core.Common;
using Okey.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Okey.Core.Configuration;
using System.Text.RegularExpressions;


namespace Okey.Core.Common
{
    class PlayerTileElementsSorter
    {

        public static string autoRegexStr = @"^(((?:A01|OK)(?:A02|OK)(?:A03|OK)(?:A04|OK)(?:A05|OK))|((?:B01|OK)(?:B02|OK)(?:B03|OK)(?:B04|OK)(?:B05|OK))|((?:C01|OK)(?:C02|OK)(?:C03|OK)(?:C04|OK)(?:C05|OK))|((?:D01|OK)(?:D02|OK)(?:D03|OK)(?:D04|OK)(?:D05|OK))|((?:A01|OK)(?:A02|OK)(?:A03|OK)(?:A04|OK))|((?:B01|OK)(?:B02|OK)(?:B03|OK)(?:B04|OK))|((?:C01|OK)(?:C02|OK)(?:C03|OK)(?:C04|OK))|((?:D01|OK)(?:D02|OK)(?:D03|OK)(?:D04|OK))|((?:A01|OK)(?:B01|OK)(?:C01|OK)(?:D01|OK))|((?:B01|OK)(?:C01|OK)(?:D01|OK))|((?:A01|OK)(?:C01|OK)(?:D01|OK))|((?:A01|OK)(?:B01|OK)(?:D01|OK))|((?:A01|OK)(?:B01|OK)(?:C01|OK))|((?:A01|OK)(?:A02|OK)(?:A03|OK))|((?:B01|OK)(?:B02|OK)(?:B03|OK))|((?:C01|OK)(?:C02|OK)(?:C03|OK))|((?:D01|OK)(?:D02|OK)(?:D03|OK))|((?:A02|OK)(?:A03|OK)(?:A04|OK)(?:A05|OK)(?:A06|OK))|((?:B02|OK)(?:B03|OK)(?:B04|OK)(?:B05|OK)(?:B06|OK))|((?:C02|OK)(?:C03|OK)(?:C04|OK)(?:C05|OK)(?:C06|OK))|((?:D02|OK)(?:D03|OK)(?:D04|OK)(?:D05|OK)(?:D06|OK))|((?:A02|OK)(?:A03|OK)(?:A04|OK)(?:A05|OK))|((?:B02|OK)(?:B03|OK)(?:B04|OK)(?:B05|OK))|((?:C02|OK)(?:C03|OK)(?:C04|OK)(?:C05|OK))|((?:D02|OK)(?:D03|OK)(?:D04|OK)(?:D05|OK))|((?:A02|OK)(?:B02|OK)(?:C02|OK)(?:D02|OK))|((?:B02|OK)(?:C02|OK)(?:D02|OK))|((?:A02|OK)(?:C02|OK)(?:D02|OK))|((?:A02|OK)(?:B02|OK)(?:D02|OK))|((?:A02|OK)(?:B02|OK)(?:C02|OK))|((?:A02|OK)(?:A03|OK)(?:A04|OK))|((?:B02|OK)(?:B03|OK)(?:B04|OK))|((?:C02|OK)(?:C03|OK)(?:C04|OK))|((?:D02|OK)(?:D03|OK)(?:D04|OK))|((?:A03|OK)(?:A04|OK)(?:A05|OK)(?:A06|OK)(?:A07|OK))|((?:B03|OK)(?:B04|OK)(?:B05|OK)(?:B06|OK)(?:B07|OK))|((?:C03|OK)(?:C04|OK)(?:C05|OK)(?:C06|OK)(?:C07|OK))|((?:D03|OK)(?:D04|OK)(?:D05|OK)(?:D06|OK)(?:D07|OK))|((?:A03|OK)(?:A04|OK)(?:A05|OK)(?:A06|OK))|((?:B03|OK)(?:B04|OK)(?:B05|OK)(?:B06|OK))|((?:C03|OK)(?:C04|OK)(?:C05|OK)(?:C06|OK))|((?:D03|OK)(?:D04|OK)(?:D05|OK)(?:D06|OK))|((?:A03|OK)(?:B03|OK)(?:C03|OK)(?:D03|OK))|((?:B03|OK)(?:C03|OK)(?:D03|OK))|((?:A03|OK)(?:C03|OK)(?:D03|OK))|((?:A03|OK)(?:B03|OK)(?:D03|OK))|((?:A03|OK)(?:B03|OK)(?:C03|OK))|((?:A03|OK)(?:A04|OK)(?:A05|OK))|((?:B03|OK)(?:B04|OK)(?:B05|OK))|((?:C03|OK)(?:C04|OK)(?:C05|OK))|((?:D03|OK)(?:D04|OK)(?:D05|OK))|((?:A04|OK)(?:A05|OK)(?:A06|OK)(?:A07|OK)(?:A08|OK))|((?:B04|OK)(?:B05|OK)(?:B06|OK)(?:B07|OK)(?:B08|OK))|((?:C04|OK)(?:C05|OK)(?:C06|OK)(?:C07|OK)(?:C08|OK))|((?:D04|OK)(?:D05|OK)(?:D06|OK)(?:D07|OK)(?:D08|OK))|((?:A04|OK)(?:A05|OK)(?:A06|OK)(?:A07|OK))|((?:B04|OK)(?:B05|OK)(?:B06|OK)(?:B07|OK))|((?:C04|OK)(?:C05|OK)(?:C06|OK)(?:C07|OK))|((?:D04|OK)(?:D05|OK)(?:D06|OK)(?:D07|OK))|((?:A04|OK)(?:B04|OK)(?:C04|OK)(?:D04|OK))|((?:B04|OK)(?:C04|OK)(?:D04|OK))|((?:A04|OK)(?:C04|OK)(?:D04|OK))|((?:A04|OK)(?:B04|OK)(?:D04|OK))|((?:A04|OK)(?:B04|OK)(?:C04|OK))|((?:A04|OK)(?:A05|OK)(?:A06|OK))|((?:B04|OK)(?:B05|OK)(?:B06|OK))|((?:C04|OK)(?:C05|OK)(?:C06|OK))|((?:D04|OK)(?:D05|OK)(?:D06|OK))|((?:A05|OK)(?:A06|OK)(?:A07|OK)(?:A08|OK)(?:A09|OK))|((?:B05|OK)(?:B06|OK)(?:B07|OK)(?:B08|OK)(?:B09|OK))|((?:C05|OK)(?:C06|OK)(?:C07|OK)(?:C08|OK)(?:C09|OK))|((?:D05|OK)(?:D06|OK)(?:D07|OK)(?:D08|OK)(?:D09|OK))|((?:A05|OK)(?:A06|OK)(?:A07|OK)(?:A08|OK))|((?:B05|OK)(?:B06|OK)(?:B07|OK)(?:B08|OK))|((?:C05|OK)(?:C06|OK)(?:C07|OK)(?:C08|OK))|((?:D05|OK)(?:D06|OK)(?:D07|OK)(?:D08|OK))|((?:A05|OK)(?:B05|OK)(?:C05|OK)(?:D05|OK))|((?:B05|OK)(?:C05|OK)(?:D05|OK))|((?:A05|OK)(?:C05|OK)(?:D05|OK))|((?:A05|OK)(?:B05|OK)(?:D05|OK))|((?:A05|OK)(?:B05|OK)(?:C05|OK))|((?:A05|OK)(?:A06|OK)(?:A07|OK))|((?:B05|OK)(?:B06|OK)(?:B07|OK))|((?:C05|OK)(?:C06|OK)(?:C07|OK))|((?:D05|OK)(?:D06|OK)(?:D07|OK))|((?:A06|OK)(?:A07|OK)(?:A08|OK)(?:A09|OK)(?:A10|OK))|((?:B06|OK)(?:B07|OK)(?:B08|OK)(?:B09|OK)(?:B10|OK))|((?:C06|OK)(?:C07|OK)(?:C08|OK)(?:C09|OK)(?:C10|OK))|((?:D06|OK)(?:D07|OK)(?:D08|OK)(?:D09|OK)(?:D10|OK))|((?:A06|OK)(?:A07|OK)(?:A08|OK)(?:A09|OK))|((?:B06|OK)(?:B07|OK)(?:B08|OK)(?:B09|OK))|((?:C06|OK)(?:C07|OK)(?:C08|OK)(?:C09|OK))|((?:D06|OK)(?:D07|OK)(?:D08|OK)(?:D09|OK))|((?:A06|OK)(?:B06|OK)(?:C06|OK)(?:D06|OK))|((?:B06|OK)(?:C06|OK)(?:D06|OK))|((?:A06|OK)(?:C06|OK)(?:D06|OK))|((?:A06|OK)(?:B06|OK)(?:D06|OK))|((?:A06|OK)(?:B06|OK)(?:C06|OK))|((?:A06|OK)(?:A07|OK)(?:A08|OK))|((?:B06|OK)(?:B07|OK)(?:B08|OK))|((?:C06|OK)(?:C07|OK)(?:C08|OK))|((?:D06|OK)(?:D07|OK)(?:D08|OK))|((?:A07|OK)(?:A08|OK)(?:A09|OK)(?:A10|OK)(?:A11|OK))|((?:B07|OK)(?:B08|OK)(?:B09|OK)(?:B10|OK)(?:B11|OK))|((?:C07|OK)(?:C08|OK)(?:C09|OK)(?:C10|OK)(?:C11|OK))|((?:D07|OK)(?:D08|OK)(?:D09|OK)(?:D10|OK)(?:D11|OK))|((?:A07|OK)(?:A08|OK)(?:A09|OK)(?:A10|OK))|((?:B07|OK)(?:B08|OK)(?:B09|OK)(?:B10|OK))|((?:C07|OK)(?:C08|OK)(?:C09|OK)(?:C10|OK))|((?:D07|OK)(?:D08|OK)(?:D09|OK)(?:D10|OK))|((?:A07|OK)(?:B07|OK)(?:C07|OK)(?:D07|OK))|((?:B07|OK)(?:C07|OK)(?:D07|OK))|((?:A07|OK)(?:C07|OK)(?:D07|OK))|((?:A07|OK)(?:B07|OK)(?:D07|OK))|((?:A07|OK)(?:B07|OK)(?:C07|OK))|((?:A07|OK)(?:A08|OK)(?:A09|OK))|((?:B07|OK)(?:B08|OK)(?:B09|OK))|((?:C07|OK)(?:C08|OK)(?:C09|OK))|((?:D07|OK)(?:D08|OK)(?:D09|OK))|((?:A08|OK)(?:A09|OK)(?:A10|OK)(?:A11|OK)(?:A12|OK))|((?:B08|OK)(?:B09|OK)(?:B10|OK)(?:B11|OK)(?:B12|OK))|((?:C08|OK)(?:C09|OK)(?:C10|OK)(?:C11|OK)(?:C12|OK))|((?:D08|OK)(?:D09|OK)(?:D10|OK)(?:D11|OK)(?:D12|OK))|((?:A08|OK)(?:A09|OK)(?:A10|OK)(?:A11|OK))|((?:B08|OK)(?:B09|OK)(?:B10|OK)(?:B11|OK))|((?:C08|OK)(?:C09|OK)(?:C10|OK)(?:C11|OK))|((?:D08|OK)(?:D09|OK)(?:D10|OK)(?:D11|OK))|((?:A08|OK)(?:B08|OK)(?:C08|OK)(?:D08|OK))|((?:B08|OK)(?:C08|OK)(?:D08|OK))|((?:A08|OK)(?:C08|OK)(?:D08|OK))|((?:A08|OK)(?:B08|OK)(?:D08|OK))|((?:A08|OK)(?:B08|OK)(?:C08|OK))|((?:A08|OK)(?:A09|OK)(?:A10|OK))|((?:B08|OK)(?:B09|OK)(?:B10|OK))|((?:C08|OK)(?:C09|OK)(?:C10|OK))|((?:D08|OK)(?:D09|OK)(?:D10|OK))|((?:A09|OK)(?:A10|OK)(?:A11|OK)(?:A12|OK)(?:A13|OK))|((?:B09|OK)(?:B10|OK)(?:B11|OK)(?:B12|OK)(?:B13|OK))|((?:C09|OK)(?:C10|OK)(?:C11|OK)(?:C12|OK)(?:C13|OK))|((?:D09|OK)(?:D10|OK)(?:D11|OK)(?:D12|OK)(?:D13|OK))|((?:A09|OK)(?:A10|OK)(?:A11|OK)(?:A12|OK))|((?:B09|OK)(?:B10|OK)(?:B11|OK)(?:B12|OK))|((?:C09|OK)(?:C10|OK)(?:C11|OK)(?:C12|OK))|((?:D09|OK)(?:D10|OK)(?:D11|OK)(?:D12|OK))|((?:A09|OK)(?:B09|OK)(?:C09|OK)(?:D09|OK))|((?:B09|OK)(?:C09|OK)(?:D09|OK))|((?:A09|OK)(?:C09|OK)(?:D09|OK))|((?:A09|OK)(?:B09|OK)(?:D09|OK))|((?:A09|OK)(?:B09|OK)(?:C09|OK))|((?:A09|OK)(?:A10|OK)(?:A11|OK))|((?:B09|OK)(?:B10|OK)(?:B11|OK))|((?:C09|OK)(?:C10|OK)(?:C11|OK))|((?:D09|OK)(?:D10|OK)(?:D11|OK))|((?:A10|OK)(?:A11|OK)(?:A12|OK)(?:A13|OK))|((?:B10|OK)(?:B11|OK)(?:B12|OK)(?:B13|OK))|((?:C10|OK)(?:C11|OK)(?:C12|OK)(?:C13|OK))|((?:D10|OK)(?:D11|OK)(?:D12|OK)(?:D13|OK))|((?:A10|OK)(?:B10|OK)(?:C10|OK)(?:D10|OK))|((?:B10|OK)(?:C10|OK)(?:D10|OK))|((?:A10|OK)(?:C10|OK)(?:D10|OK))|((?:A10|OK)(?:B10|OK)(?:D10|OK))|((?:A10|OK)(?:B10|OK)(?:C10|OK))|((?:A10|OK)(?:A11|OK)(?:A12|OK))|((?:B10|OK)(?:B11|OK)(?:B12|OK))|((?:C10|OK)(?:C11|OK)(?:C12|OK))|((?:D10|OK)(?:D11|OK)(?:D12|OK))|((?:A11|OK)(?:B11|OK)(?:C11|OK)(?:D11|OK))|((?:B11|OK)(?:C11|OK)(?:D11|OK))|((?:A11|OK)(?:C11|OK)(?:D11|OK))|((?:A11|OK)(?:B11|OK)(?:D11|OK))|((?:A11|OK)(?:B11|OK)(?:C11|OK))|((?:A11|OK)(?:A12|OK)(?:A13|OK))|((?:B11|OK)(?:B12|OK)(?:B13|OK))|((?:C11|OK)(?:C12|OK)(?:C13|OK))|((?:D11|OK)(?:D12|OK)(?:D13|OK))|((?:A12|OK)(?:B12|OK)(?:C12|OK)(?:D12|OK))|((?:B12|OK)(?:C12|OK)(?:D12|OK))|((?:A12|OK)(?:C12|OK)(?:D12|OK))|((?:A12|OK)(?:B12|OK)(?:D12|OK))|((?:A12|OK)(?:B12|OK)(?:C12|OK))|((?:A13|OK)(?:B13|OK)(?:C13|OK)(?:D13|OK))|((?:B13|OK)(?:C13|OK)(?:D13|OK))|((?:A13|OK)(?:C13|OK)(?:D13|OK))|((?:A13|OK)(?:B13|OK)(?:D13|OK))|((?:A13|OK)(?:B13|OK)(?:C13|OK)))+";
        //public static string autoRegexStr = @"^(((?:A01|OK)(?:A02|OK)(?:A03|OK)(?:A04|OK)(?:A05|OK))|((?:B01|OK)(?:B02|OK)(?:B03|OK)(?:B04|OK)(?:B05|OK)))+";
        public static string bestMatchStr = "";
        public static List<TileElement> availableTiles = new List<TileElement>();
        public static string pairableTiles = "";
        public static List<List<TileElement>> groups = new List<List<TileElement>>();
        public static int bestMatchLength = -1;
        private static int mode = 0;
        public static Regex rgx = new Regex(autoRegexStr);
        public static int cyleCount = 0;
        public static int maxPair = 0;

        public static void Sort(SortMode mode)
        {
            switch (mode)
            {
                case SortMode.Color:
                    {
                        SortByColor();
                        break;
                    }
                case SortMode.Number:
                    {
                        SortByNumber();
                        break;
                    }
                case SortMode.Auto:
                    {
                        SortByRegex();
                        break;
                    }
            }
        }


        public static void SortByColorComparer()
        {
            TileElementComparerColor colorComparer = new TileElementComparerColor();
            GameData.GD.playerTiles.Sort(colorComparer);
        }

        public static void SortByNumberComparer()
        {
            TileElementComparerNumber numberComparer = new TileElementComparerNumber();
            GameData.GD.playerTiles.Sort(numberComparer);
        }

        public static void SortByNumberAndColor()
        {
            TileElementComparerNumber numberComparer = new TileElementComparerNumber();
            GameData.GD.playerTiles.Sort(numberComparer);
            TileElementComparerColor colorComparer = new TileElementComparerColor();
            GameData.GD.playerTiles.Sort(colorComparer);
        }

        public static void SortByColor()
        {
            SortByColorComparer();
            availableTiles = new List<TileElement>();
            groups = new List<List<TileElement>>();

            for (int tileIndex = 0; tileIndex < GameData.GD.playerTiles.Count; tileIndex++)
            {
                availableTiles.Add(GameData.GD.playerTiles[tileIndex]);
            }

            bool changed = true;
            while (changed)
            {
                changed = false;
                List<TileElement> lastMatched = new List<TileElement>();
                if (availableTiles.Count > 0)
                {
                    lastMatched.Add(availableTiles[0]);
                }
                for (int i = 1; i < availableTiles.Count; i++)
                {
                    if (availableTiles[i].color == lastMatched[lastMatched.Count - 1].color)
                    {
                        if (availableTiles[i].number > lastMatched[lastMatched.Count - 1].number + 1)
                        {
                            if (lastMatched.Count >= 3)
                            {
                                break;
                            }
                            else
                            {

                                lastMatched.Clear();
                                lastMatched.Add(availableTiles[i]);
                            }
                        }
                        else if (availableTiles[i].number == lastMatched[lastMatched.Count - 1].number + 1
                            || (Conf.configuration.gameConf.oneAfterLast && availableTiles[i].number == 1
                            && lastMatched[lastMatched.Count - 1].number == Conf.configuration.gameConf.numberCount + 1))
                        {
                            lastMatched.Add(availableTiles[i]);
                        }
                    }
                    else
                    {
                        if (lastMatched.Count >= 3)
                        {
                            break;
                        }
                        else
                        {

                            lastMatched.Clear();
                            lastMatched.Add(availableTiles[i]);
                        }
                    }
                }
                if (lastMatched.Count >= 3)
                {
                    List<TileElement> group = new List<TileElement>();
                    for (int cnt = 0; cnt < lastMatched.Count; cnt++)
                    {
                        availableTiles.Remove(lastMatched[cnt]);
                        group.Add(lastMatched[cnt]);
                    }
                    groups.Add(group);
                    changed = true;
                }
                for (int i = 0; i < availableTiles.Count && !changed; i++)
                {
                    for (int g = 0; g < groups.Count; g++)
                    {
                        TileElement groupFirstElement = groups[g][0];
                        if (groupFirstElement.color == availableTiles[i].color)
                        {
                            if (groupFirstElement.number == availableTiles[i].number + 1)
                            {
                                groups[g].Insert(0, availableTiles[i]);
                                availableTiles.Remove(availableTiles[i]);
                                changed = true;
                                break;
                            }
                            else if (groupFirstElement.number + groups[g].Count == availableTiles[i].number
                            || (Conf.configuration.gameConf.oneAfterLast && availableTiles[i].number == 1
                            && groupFirstElement.number + groups[g].Count == Conf.configuration.gameConf.numberCount + 1))
                            {
                                groups[g].Add(availableTiles[i]);
                                availableTiles.Remove(availableTiles[i]);
                                changed = true;
                                break;
                            }
                            else if (groupFirstElement.number < availableTiles[i].number - 1
                                && groupFirstElement.number + groups[g].Count > availableTiles[i].number + 2)
                            {
                                List<TileElement> group = new List<TileElement>();
                                int index = -1;
                                for (int gi = 0; gi < groups[g].Count; gi++)
                                {
                                    if (groups[g][gi].number > availableTiles[i].number)
                                    {
                                        if (!changed)
                                        {
                                            group.Add(availableTiles[i]);
                                            changed = true;
                                            index = gi;
                                        }
                                        group.Add(groups[g][gi]);
                                    }
                                }
                                if (index != -1)
                                {
                                    groups[g].RemoveRange(index, groups[g].Count - index);
                                    groups.Add(group);
                                    availableTiles.Remove(availableTiles[i]);
                                    changed = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (Conf.configuration.gameConf.oneAfterLast)
            {
                List<List<TileElement>> colorsNumbers = new List<List<TileElement>>();
                for (int i = 0; i < Conf.configuration.gameConf.colorCount; i++)
                {
                    colorsNumbers.Add(new List<TileElement>());
                }
                for (int tileIndex = 0; tileIndex < availableTiles.Count; tileIndex++)
                {
                    if (availableTiles[tileIndex].number == 1)
                    {
                        int color = (int)availableTiles[tileIndex].color;
                        if (colorsNumbers[color].Count == 0)
                        {
                            colorsNumbers[color].Add(availableTiles[tileIndex]);
                        }
                        else if (colorsNumbers[color].Count == 1 && colorsNumbers[color][0].number != availableTiles[tileIndex].number)
                        {
                            colorsNumbers[color].Add(availableTiles[tileIndex]);
                        }
                        else if (colorsNumbers[color].Count == 2 && colorsNumbers[color][0].number != availableTiles[tileIndex].number
                            && colorsNumbers[color][1].number != availableTiles[tileIndex].number)
                        {
                            colorsNumbers[color].Add(availableTiles[tileIndex]);
                        }
                    }
                    if (availableTiles[tileIndex].number == Conf.configuration.gameConf.numberCount)
                    {
                        int color = (int)availableTiles[tileIndex].color;
                        if (colorsNumbers[color].Count == 0)
                        {
                            colorsNumbers[color].Add(availableTiles[tileIndex]);
                        }
                        else if (colorsNumbers[color].Count == 1 && colorsNumbers[color][0].number != availableTiles[tileIndex].number)
                        {
                            if (colorsNumbers[color][0].number == 1)
                            {
                                colorsNumbers[color].Insert(0, availableTiles[tileIndex]);
                            }
                            else
                            {
                                colorsNumbers[color].Add(availableTiles[tileIndex]);
                            }
                        }
                        else if (colorsNumbers[color].Count == 2 && colorsNumbers[color][0].number != availableTiles[tileIndex].number
                             && colorsNumbers[color][1].number != availableTiles[tileIndex].number)
                        {
                            colorsNumbers[color].Insert(1, availableTiles[tileIndex]);
                        }
                    }
                    if (availableTiles[tileIndex].number == Conf.configuration.gameConf.numberCount - 1)
                    {
                        int color = (int)availableTiles[tileIndex].color;
                        if (colorsNumbers[color].Count == 0)
                        {
                            colorsNumbers[color].Add(availableTiles[tileIndex]);
                        }
                        else if (colorsNumbers[color].Count == 1 && colorsNumbers[color][0].number != availableTiles[tileIndex].number)
                        {
                            colorsNumbers[color].Insert(0, availableTiles[tileIndex]);
                        }
                        else if (colorsNumbers[color].Count == 2 && colorsNumbers[color][0].number != availableTiles[tileIndex].number
                             && colorsNumbers[color][1].number != availableTiles[tileIndex].number)
                        {
                            colorsNumbers[color].Insert(0, availableTiles[tileIndex]);
                        }
                    }
                }
                for (int c = 0; c < colorsNumbers.Count; c++)
                {
                    if (colorsNumbers[c].Count == 2)
                    {
                        for (int g = 0; g < groups.Count; g++)
                        {
                            if (groups[g].Count > 3 && (int)groups[g][0].color == c && groups[g][0].number == 1)
                            {
                                List<TileElement> group = new List<TileElement>();
                                group.Add(colorsNumbers[c][0]);
                                group.Add(colorsNumbers[c][1]);
                                group.Add(groups[g][0]);
                                groups[g].RemoveAt(0);
                                groups.Add(group);
                                availableTiles.Remove(colorsNumbers[c][0]);
                                availableTiles.Remove(colorsNumbers[c][1]);
                                break;
                            }
                        }
                    }
                    else if (colorsNumbers[c].Count == 3)
                    {
                        List<TileElement> group = new List<TileElement>();
                        for (int cn = 0; cn < colorsNumbers[c].Count; cn++)
                        {
                            group.Add(colorsNumbers[c][cn]);
                            availableTiles.Remove(colorsNumbers[c][cn]);
                        }
                        groups.Add(group);
                    }
                }
            }

            GameData.GD.remainingTileCount = availableTiles.Count;
            GameData.GD.availableTiles = availableTiles;
            GameData.GD.unpairedTiles = new List<TileElement>();
            GameData.GD.groups = groups;
        }


        public static void SortByNumber()
        {
            SortByNumberComparer();

            List<TileElement> availableTiles = new List<TileElement>();
            List<List<TileElement>> groups = new List<List<TileElement>>();

            for (int tileIndex = 0; tileIndex < GameData.GD.playerTiles.Count; tileIndex++)
            {
                availableTiles.Add(GameData.GD.playerTiles[tileIndex]);
            }

            bool changed = true;
            while (changed)
            {
                changed = false;
                List<TileElement> lastMatched = new List<TileElement>();
                if (availableTiles.Count > 0)
                {
                    lastMatched.Add(availableTiles[0]);
                }
                for (int i = 1; i < availableTiles.Count; i++)
                {
                    if (availableTiles[i].number == lastMatched[lastMatched.Count - 1].number)
                    {
                        if (availableTiles[i].color != lastMatched[lastMatched.Count - 1].color)
                        {
                            lastMatched.Add(availableTiles[i]);
                        }
                    }
                    else
                    {
                        if (lastMatched.Count >= 3)
                        {
                            break;
                        }
                        else
                        {
                            lastMatched.Clear();
                            lastMatched.Add(availableTiles[i]);
                        }
                    }
                }

                if (lastMatched.Count >= 3)
                {
                    List<TileElement> group = new List<TileElement>();
                    for (int cnt = 0; cnt < lastMatched.Count; cnt++)
                    {
                        availableTiles.Remove(lastMatched[cnt]);
                        group.Add(lastMatched[cnt]);
                    }
                    groups.Add(group);
                    changed = true;
                }
                if (!changed && availableTiles.Count > 0)
                {
                    lastMatched.Clear();
                    lastMatched.Add(availableTiles[0]);

                    for (int i = 1; i < availableTiles.Count && !changed; i++)
                    {
                        if (availableTiles[i].number == lastMatched[lastMatched.Count - 1].number)
                        {
                            if (availableTiles[i].color != lastMatched[lastMatched.Count - 1].color)
                            {
                                lastMatched.Add(availableTiles[i]);
                            }
                        }
                        else
                        {
                            if (lastMatched.Count == 2)
                            {
                                for (int gr = 0; gr < groups.Count && !changed; gr++)
                                {
                                    if (groups[gr][0].number == lastMatched[0].number &&
                                        groups[gr].Count == 4)
                                    {
                                        for (int gi = 0; gi < groups[gr].Count; gi++)
                                        {
                                            if (groups[gr][gi].color != lastMatched[0].color &&
                                                groups[gr][gi].color != lastMatched[1].color)
                                            {
                                                List<TileElement> group = new List<TileElement>();
                                                group.Add(lastMatched[0]);
                                                group.Add(lastMatched[1]);
                                                group.Add(groups[gr][gi]);
                                                availableTiles.Remove(lastMatched[0]);
                                                availableTiles.Remove(lastMatched[1]);
                                                groups[gr].Remove(groups[gr][gi]);
                                                groups.Add(group);
                                                changed = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                lastMatched.Clear();
                                lastMatched.Add(availableTiles[i]);
                            }
                            else
                            {
                                lastMatched.Clear();
                                lastMatched.Add(availableTiles[i]);
                            }
                        }
                    }
                    if (lastMatched.Count == 2)
                    {
                        for (int gr = 0; gr < groups.Count && !changed; gr++)
                        {
                            if (groups[gr][0].number == lastMatched[0].number &&
                                groups[gr].Count == 4)
                            {
                                for (int gi = 0; gi < groups[gr].Count; gi++)
                                {
                                    if (groups[gr][gi].color != lastMatched[0].color &&
                                        groups[gr][gi].color != lastMatched[1].color)
                                    {
                                        List<TileElement> group = new List<TileElement>();
                                        group.Add(lastMatched[0]);
                                        group.Add(lastMatched[1]);
                                        group.Add(groups[gr][gi]);
                                        availableTiles.Remove(lastMatched[0]);
                                        availableTiles.Remove(lastMatched[1]);
                                        groups[gr].Remove(groups[gr][gi]);
                                        groups.Add(group);
                                        changed = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            GameData.GD.remainingTileCount = availableTiles.Count;
            GameData.GD.availableTiles = availableTiles;
            GameData.GD.unpairedTiles = new List<TileElement>();
            GameData.GD.groups = groups;
        }

        private static void SortByRegex()
        {
            string tilesStr = "";
            for(int i = 0; i < GameData.GD.playerTiles.Count; i++)
            {
                tilesStr += GameData.GD.playerTiles[i].RegexKey();
            }
            //rgx = new Regex(RegexCreator());
            pairableTiles = "";
            string regexStr = FilteredRegexCreator(tilesStr);
            if (string.IsNullOrEmpty(pairableTiles))
            {

                GameData.GD.remainingTileCount = GameData.GD.playerTiles.Count;
                GameData.GD.availableTiles = GameData.GD.playerTiles;
                GameData.GD.groups = new List<List<TileElement>>();
            }
            else
            {

                rgx = new Regex(regexStr);
                SortByColorComparer();
                availableTiles = new List<TileElement>();
                List<TileElement> remainingTiles = new List<TileElement>();
                for (int i = 0; i < GameData.GD.playerTiles.Count; i++)
                {
                    if (pairableTiles.Contains(GameData.GD.playerTiles[i].RegexKey())
                        || GameData.GD.playerTiles[i].RegexKey().Equals("OK"))
                    {
                        availableTiles.Add(GameData.GD.playerTiles[i]);
                    }
                    else
                    {
                        remainingTiles.Add(GameData.GD.playerTiles[i]);
                    }
                }
                groups = new List<List<TileElement>>();

                bestMatchLength = -1;
                bestMatchStr = "";
                cyleCount = 0;
                maxPair = availableTiles.Count;
                Regexsive(new List<TileElement>(), "", new List<TileElement>(availableTiles));

                GameData.GD.remainingTileCount = availableTiles.Count + remainingTiles.Count;
                GameData.GD.availableTiles = availableTiles;
                GameData.GD.unpairedTiles = remainingTiles;
                GameData.GD.groups = groups;
            }
        }


        private static void Regexsive(List<TileElement> usedTiles, string str, List<TileElement> tiles)
        {
            cyleCount++;
            if (bestMatchLength == maxPair*3 || cyleCount > 5000000)
            {
                return;
            }
            if(tiles != null && tiles.Count > 0)
            {
                for(int i = 0; i < tiles.Count; i++)
                {
                    string regularString = str + tiles[i].RegexKey();
                    List<TileElement> newTilesList = new List<TileElement>(tiles);
                    newTilesList.RemoveAt(i);
                    List<TileElement> newUsedList = new List<TileElement>(usedTiles);
                    newUsedList.Add(tiles[i]);
                    if (regularString.Length > 6)
                    {

                        Match mtch = rgx.Match(regularString);
                        if (mtch.Length > bestMatchLength)
                        {
                            bestMatchLength = mtch.Length;
                            bestMatchStr = regularString;
                            availableTiles = new List<TileElement>(newTilesList);
                            groups.Clear();
                            for(int g = 1; g < mtch.Groups.Count; g++)
                            {
                                if(!string.IsNullOrEmpty(mtch.Groups[g].Value))
                                {
                                    List<TileElement> group = new List<TileElement>();
                                    string groupStr = mtch.Groups[g].Value;
                                    while(groupStr.Length > 0)
                                    {
                                        for(int a = 0; a < newUsedList.Count; a++)
                                        {
                                            if(groupStr.Contains(newUsedList[a].RegexKey()))
                                            {
                                                groupStr = groupStr.Replace(newUsedList[a].RegexKey(), "");
                                                group.Add(newUsedList[a]);
                                                break;
                                            }
                                        }
                                    }
                                    groups.Add(group);
                                }
                            }
                        }
                        else if (regularString.Length - mtch.Length > 9
                            //bestMatchLength - mtch.Length > 12 
                            || regularString.Length - bestMatchLength > 9)
                        {
                            break;
                        }
                    }

                    if (newTilesList.Count > 0)
                    {
                        Regexsive(newUsedList, regularString, newTilesList);
                    }
                }
            }
        }
        private static string RegexCreator()
        {
            string regex = @"^(?:";

            for (int n = 1; n <= Conf.configuration.gameConf.numberCount; n++)
            {
                int length = 5;
                if (n + length <= Conf.configuration.gameConf.numberCount + 1)
                {
                    for (int c = 0; c < Conf.configuration.gameConf.colorCount; c++)
                    {
                        regex += "(";
                        for (int i = n; i <= Conf.configuration.gameConf.numberCount && i < n + length; i++)
                        {
                            regex += "(?:" + ((char)(c + 65)) + i.ToString("D2") + "|OK)";
                        }
                        regex += ")|";
                    }
                }

                length = 4;
                if (n + length <= Conf.configuration.gameConf.numberCount + 1)
                {
                    for (int c = 0; c < Conf.configuration.gameConf.colorCount; c++)
                    {
                        regex += "(";
                        for (int i = n; i <= Conf.configuration.gameConf.numberCount && i < n + length; i++)
                        {
                            regex += "(?:" + ((char)(c + 65)) + i.ToString("D2") + "|OK)";
                        }
                        regex += ")|";
                    }
                }

                // A1B1C1D1
                regex += "(";
                for (int j = 0; j < Conf.configuration.gameConf.colorCount; j++)
                {
                    regex += "(?:" + ((char)(j + 65)) + n.ToString("D2") + "|OK)";
                }
                regex += ")|";

                //A1C1D1 - B3C3D3
                for (int i = 0; i < Conf.configuration.gameConf.colorCount; i++)
                {
                    regex += "(";
                    for (int j = 0; j < Conf.configuration.gameConf.colorCount; j++)
                    {
                        if (i != j)
                        {
                            regex += "(?:" + ((char)(j + 65)) + n.ToString("D2") + "|OK)";
                        }
                    }
                    regex += ")|";
                }

                length = 3;
                if (n + length <= Conf.configuration.gameConf.numberCount + 1)
                {
                    for (int c = 0; c < Conf.configuration.gameConf.colorCount; c++)
                    {
                        regex += "(";
                        for (int i = n; i <= Conf.configuration.gameConf.numberCount && i < n + length; i++)
                        {
                            regex += "(?:" + ((char)(c + 65)) + i.ToString("D2") + "|OK)";
                        }
                        regex += ")|";
                    }
                }

            }
            regex = regex.Substring(0, regex.Length - 1) + ")+";
            return regex;
        }

        private static string FilteredRegexCreator(string tiles)
        {
            string regex = @"^(?:";
            string groupRegex = "";
            int tolerance = 0;
            int toleranceLimit = 0;
            int oneAfterLast = Conf.configuration.gameConf.oneAfterLast ? 1 : 0;
            List<string> pairGroup = new List<string>();
            if(tiles.Contains("OK"))
            {
                toleranceLimit = 1;
            }

            for (int n = 0; n < Conf.configuration.gameConf.numberCount; n++)
            {
                int length = 5;
                if (n + length < Conf.configuration.gameConf.numberCount + 1 + oneAfterLast)
                {
                    for (int c = 0; c < Conf.configuration.gameConf.colorCount; c++)
                    {
                        tolerance = 0;
                        groupRegex = "(";
                        pairGroup.Clear();
                        for (int i = n; i < Conf.configuration.gameConf.numberCount + oneAfterLast && i < n + length; i++)
                        {
                            string key = ((char)(c + 65)) + ((i % Conf.configuration.gameConf.numberCount) + 1).ToString("D2");
                            if (!tiles.Contains(key))
                            {
                                tolerance++;
                                if (tolerance > toleranceLimit)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                pairGroup.Add(key);
                            }
                            groupRegex += "(?:" + key + "|OK)";
                        }
                        groupRegex += ")|";
                        if (tolerance <= toleranceLimit)
                        {
                            regex += groupRegex;
                            for(int p = 0; p < pairGroup.Count; p++)
                            {
                                if(!pairableTiles.Contains(pairGroup[p]))
                                {
                                    pairableTiles += pairGroup[p];
                                }
                            }
                        }
                    }
                }
            }

            for (int n = 0; n < Conf.configuration.gameConf.numberCount; n++)
            {
                int length = 4;
                if (n + length < Conf.configuration.gameConf.numberCount + 1 + oneAfterLast)
                {
                    for (int c = 0; c < Conf.configuration.gameConf.colorCount; c++)
                    {
                        tolerance = 0;
                        groupRegex = "(";
                        pairGroup.Clear();
                        for (int i = n; i < Conf.configuration.gameConf.numberCount + oneAfterLast && i < n + length; i++)
                        {
                            string key = ((char)(c + 65)) + ((i % Conf.configuration.gameConf.numberCount) + 1).ToString("D2");
                            if (!tiles.Contains(key))
                            {
                                tolerance++;
                                if (tolerance > toleranceLimit)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                pairGroup.Add(key);
                            }
                            groupRegex += "(?:" + key + "|OK)";
                        }
                        groupRegex += ")|";
                        if (tolerance <= toleranceLimit)
                        {
                            regex += groupRegex;
                            for (int p = 0; p < pairGroup.Count; p++)
                            {
                                if (!pairableTiles.Contains(pairGroup[p]))
                                {
                                    pairableTiles += pairGroup[p];
                                }
                            }
                        }
                    }
                }

            }

            for (int n = 0; n < Conf.configuration.gameConf.numberCount; n++)
            {
                // A1B1C1D1
                tolerance = 0;
                groupRegex = "(";
                pairGroup.Clear();
                for (int j = 0; j < Conf.configuration.gameConf.colorCount; j++)
                {
                    string key = ((char)(j + 65)) + (n + 1).ToString("D2");
                    if (!tiles.Contains(key))
                    {
                        tolerance++;
                        if (tolerance > toleranceLimit)
                        {
                            break;
                        }
                    }
                    else
                    {
                        pairGroup.Add(key);
                    }
                    groupRegex += "(?:" + key + "|OK)";
                }
                groupRegex += ")|";
                if (tolerance <= toleranceLimit)
                {
                    regex += groupRegex;
                    for (int p = 0; p < pairGroup.Count; p++)
                    {
                        if (!pairableTiles.Contains(pairGroup[p]))
                        {
                            pairableTiles += pairGroup[p];
                        }
                    }
                }

            }

            for (int n = 0; n < Conf.configuration.gameConf.numberCount; n++)
            {
                //A1C1D1 - B3C3D3
                for (int i = 0; i < Conf.configuration.gameConf.colorCount; i++)
                {
                    tolerance = 0;
                    groupRegex = "(";
                    pairGroup.Clear();
                    for (int j = 0; j < Conf.configuration.gameConf.colorCount; j++)
                    {
                        if (i != j)
                        {
                            string key = ((char)(j + 65)) + (n + 1).ToString("D2");
                            if (!tiles.Contains(key))
                            {
                                tolerance++;
                                if (tolerance > toleranceLimit)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                pairGroup.Add(key);
                            }
                            groupRegex += "(?:" + key + "|OK)";
                        }
                    }
                    groupRegex += ")|";
                    if (tolerance <= toleranceLimit)
                    {
                        regex += groupRegex;
                        for (int p = 0; p < pairGroup.Count; p++)
                        {
                            if (!pairableTiles.Contains(pairGroup[p]))
                            {
                                pairableTiles += pairGroup[p];
                            }
                        }
                    }
                }
            }

            for (int n = 0; n < Conf.configuration.gameConf.numberCount; n++)
            {
                int length = 3;
                if (n + length < Conf.configuration.gameConf.numberCount + 1 + oneAfterLast)
                {
                    for (int c = 0; c < Conf.configuration.gameConf.colorCount; c++)
                    {
                        tolerance = 0;
                        groupRegex = "(";
                        pairGroup.Clear();
                        for (int i = n; i < Conf.configuration.gameConf.numberCount + oneAfterLast && i < n + length; i++)
                        {
                            string key = ((char)(c + 65)) + ((i % Conf.configuration.gameConf.numberCount) + 1).ToString("D2");
                            if (!tiles.Contains(key))
                            {
                                tolerance++;
                                if (tolerance > toleranceLimit)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                pairGroup.Add(key);
                            }
                            groupRegex += "(?:" + key + "|OK)";
                        }
                        groupRegex += ")|";
                        if (tolerance <= toleranceLimit)
                        {
                            regex += groupRegex;
                            for (int p = 0; p < pairGroup.Count; p++)
                            {
                                if (!pairableTiles.Contains(pairGroup[p]))
                                {
                                    pairableTiles += pairGroup[p];
                                }
                            }
                        }
                    }
                }

            }
            regex = regex.Substring(0, regex.Length - 1) + ")+";
            return regex;
        }
    }

    }

   