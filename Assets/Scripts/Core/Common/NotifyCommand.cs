﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Okey.Core.Common
{
    public enum NotifyCommand
    {
        Log,
        ColorSort,
        NumberSort,
        AutoSort,
        Reload,
        RemainingCountChanged
    }
}
