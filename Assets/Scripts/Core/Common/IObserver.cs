﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Okey.Core.Common
{
    public interface IObserver
    {
        void Notify(NotifyCommand command, object data = null);

    }
}
