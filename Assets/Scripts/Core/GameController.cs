﻿using System;
using System.Collections.Generic;
using Okey.Core.Models;
using Okey.Core.Common;

namespace Okey.Core
{
    public class GameController : IObserver
    {
        public IObserver controller;

        public void InitGame()
        {
            int seed = RNG.Next();
            //seed = 1998584587;
            //controller.Notify(NotifyCommand.Log, seed);
            RNG.SetSeed(seed);
            GameSetup.SetupGame(Configuration.Conf.configuration);
            controller.Notify(NotifyCommand.RemainingCountChanged);

        }
        
        public void SortPlayerTileElements(SortMode mode)
        {
            PlayerTileElementsSorter.Sort(mode);
            controller.Notify(NotifyCommand.RemainingCountChanged);
        }


        public void Notify(NotifyCommand command, object data = null)
        {

        }

    }
}
