﻿using Okey.Core.Configuration;
using Okey.Core.Models;
using Okey.Core.Common;

namespace Okey.Core
{
    class GameSetup
    {
        public static void SetupGame(Conf conf)
        {
            // fill the unused tiles with all possibilities
            int totalTileCount = conf.gameConf.colorCount * conf.gameConf.numberCount * conf.gameConf.dublicationCount;
            GameData.GD.unusedTiles.Clear();
            GameData.GD.groups.Clear();
            GameData.GD.availableTiles.Clear();

            for (int tileIndex = 0; tileIndex < totalTileCount; tileIndex++)
            {
                TileElement tileElement = new TileElement();
                tileElement.index = tileIndex;
                tileElement.color = (TileColor)((int)(tileIndex / conf.gameConf.numberCount) % conf.gameConf.colorCount);
                tileElement.number = (tileIndex % conf.gameConf.numberCount) + 1;
                tileElement.isWildTile = false;
                tileElement.isReplacementTile = false;

                GameData.GD.unusedTiles.Add(tileElement);
            }
            TileElementComparerRandom randomComparer = new TileElementComparerRandom();
            GameData.GD.unusedTiles.Sort(randomComparer);


            //fill player tile elements list by seleting first tile count of unused tile list
            GameData.GD.playerTiles.Clear();
            for (int tileNumber = 0; tileNumber < conf.gameConf.tileCount && GameData.GD.unusedTiles.Count > 0; tileNumber++)
            {
                GameData.GD.playerTiles.Add(GameData.GD.unusedTiles[0]);
                GameData.GD.unusedTiles.RemoveAt(0);
            }
            GameData.GD.remainingTileCount = GameData.GD.playerTiles.Count;

            // set the wild card element
            if (conf.gameConf.wildTileEnabled && GameData.GD.unusedTiles.Count > 0)
            {
                GameData.GD.wildTileElement = GameData.GD.unusedTiles[0];
                for (int p = 0; p < GameData.GD.playerTiles.Count; p++)
                {
                    if (GameData.GD.playerTiles[p].RegexKey().Equals(GameData.GD.wildTileElement.RegexKey()))
                    {
                        GameData.GD.playerTiles[p].isWildTile = true;
                    }
                }
                for (int i = 0; i < GameData.GD.unusedTiles.Count; i++)
                {
                    if (GameData.GD.unusedTiles[i].RegexKey().Equals(GameData.GD.wildTileElement.RegexKey()))
                    {
                        GameData.GD.unusedTiles[i].isWildTile = true;
                    }
                }
                GameData.GD.unusedTiles.RemoveAt(0);
            }

        }
    }
}
