﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Okey.Core.Models;
using Okey.Core;
using Okey.UI;
using Okey.Core.Common;

public class MainController : MonoBehaviour, IObserver {

    private GameController gameController = new GameController();
    private UIController uiController = new UIController();
    private List<IObserver> observers = new List<IObserver>();

   

    // Use this for initialization
    void Start ()
    {
        observers.Add(gameController);
        observers.Add(uiController);
        gameController.controller = this;
        uiController.controller = this;

        uiController.InitUI();
        gameController.InitGame();
        uiController.SetTiles();
    }
	
	// Update is called once per frame
	void Update () {
        uiController.UpdateTiles();
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    public void Notify(NotifyCommand command, object data = null)
    {
        switch(command)
        {
            case NotifyCommand.ColorSort:
                {
                    ColorSort();
                    break;
                }
            case NotifyCommand.NumberSort:
                {
                    NumberSort();
                    break;
                }
            case NotifyCommand.AutoSort:
                {
                    AutoSort();
                    break;
                }
            case NotifyCommand.Reload:
                {
                    Reload();
                    break;
                }
            case NotifyCommand.RemainingCountChanged:
                {
                    uiController.Notify(command);
                    break;
                }
            case NotifyCommand.Log:
                {
                    Debug.Log(data);
                    break;
                }
        }
    }
    private void ColorSort()
    {
        gameController.SortPlayerTileElements(SortMode.Color);
        uiController.SetTiles();
    }
    private void NumberSort()
    {
        gameController.SortPlayerTileElements(SortMode.Number);
        uiController.SetTiles();
    }
    private void AutoSort()
    {
        gameController.SortPlayerTileElements(SortMode.Auto);
        uiController.SetTiles();
    }
    private void Reload()
    {
        uiController.Reset();
        gameController.InitGame();
        uiController.SetTiles();
    }
}
