﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCanvasController : MonoBehaviour {

    public Button Numbers;
    public Button Colors;
    public Button Auto;
    public Button Reload;
    public Text RemainingCount;
    public Transform TileParent;
    public GameObject WildTileElement;
}
