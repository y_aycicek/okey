﻿using UnityEngine;
using UnityEngine.UI;
using Okey.Core.Models;
using System.Collections.Generic;
using Okey.Core.Common;
using Okey.Core.Configuration;

namespace Okey.UI
{
    class UIController : IObserver
    {
        public IObserver controller;
        private Canvas mainCanvas;
        private Transform tileParent;
        private MainCanvasController mainCanvasController;
        private GameObject tilePrefab;
        private List<GameObject> playerTiles = new List<GameObject>();
        private List<TileElement> orderedTiles = new List<TileElement>();
        private List<GameObject> renderedTiles = new List<GameObject>();
        private List<Vector3> targetPos = new List<Vector3>();
        private List<float> moveDelay = new List<float>();
        private Dictionary<string, Object> resources = new Dictionary<string, Object>();

        private bool isWildTileTurnedFront = false;
        private float moveDelayMultiplier = 1.0f;
        private float clickStartTime = 0.0f;
        private int clickedIndex = -1;
        private bool dragAndDrop = false;

        public void InitUI()
        {
            mainCanvas = (Canvas)GameObject.Instantiate(Resources.Load("Prefabs/MainCanvas", typeof(GameObject)) as GameObject, Vector3.zero, Quaternion.identity).GetComponent<Canvas>();
            //resources.Add("Prefabs/TileElement", Resources.Load("Prefabs/TileElement", typeof(GameObject)) as GameObject);
            mainCanvasController = mainCanvas.GetComponent<MainCanvasController>();
            mainCanvasController.Colors.onClick.AddListener(delegate { ColorsSort(); });
            mainCanvasController.Numbers.onClick.AddListener(delegate { NumbersSort(); });
            mainCanvasController.Auto.onClick.AddListener(delegate { AutoSort(); });
            mainCanvasController.Reload.onClick.AddListener(delegate { Reload(); });
            mainCanvasController.RemainingCount.text = GameData.GD.remainingTileCount.ToString();
            tileParent = mainCanvasController.TileParent;
        }
        
        public void Reset()
        {
            playerTiles.Clear();
            orderedTiles.Clear();
            targetPos.Clear();
            moveDelay.Clear();
            renderedTiles.Clear();
        }


        public void UpdateTiles ()
        {
            for(int tl = 0; tl < playerTiles.Count; tl++)
            {
                if(moveDelay[tl] > 0)
                {
                    moveDelay[tl] -= Time.deltaTime;
                }
                else
                {
                    playerTiles[tl].transform.position = Vector3.Lerp(playerTiles[tl].transform.position, targetPos[tl], Time.deltaTime * 10.0f);
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                clickedIndex = -1;
                clickStartTime = Time.timeSinceLevelLoad;
                Vector3 mPos = Input.mousePosition;
                for (int i = 0; i < playerTiles.Count; i++)
                {
                    if (Vector3.Distance(playerTiles[i].transform.position, mPos) < (32.0f * (Screen.width / 1024f)))
                    {
                        clickedIndex = i;
                        playerTiles[i].transform.SetSiblingIndex(playerTiles.Count - 1);
                        break;
                    }
                }
                dragAndDrop = false;
            }
            if(Input.GetMouseButton(0))
            {
                if (clickedIndex != -1)
                {
                    Vector3 mPos = Input.mousePosition;
                    if (!dragAndDrop && Vector3.Distance(playerTiles[clickedIndex].transform.position, mPos) > (32.0f * (Screen.width / 1024f)))
                    {
                        dragAndDrop = true;
                        //X  (70 + tilePositionX * 68) * (Screen.width / 1024f)
                        //Y  (140 + tilePositionY * 86) * (Screen.width / 1024f)
                        int posX = (int)Mathf.RoundToInt(((targetPos[clickedIndex].x / (Screen.width / 1024f)) - 70) / 68);
                        int posY = (int)Mathf.RoundToInt(((targetPos[clickedIndex].y / (Screen.width / 1024f)) - 140) / 86);
                        renderedTiles[posX + posY * -14] = null;
                    }
                    if (dragAndDrop)
                    {
                        targetPos[clickedIndex] = mPos;
                    }
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                if(dragAndDrop)
                {
                    Vector3 mPos = Input.mousePosition;

                    int posX = (int)Mathf.RoundToInt(((mPos.x / (Screen.width / 1024f)) - 70) / 68);
                    int posY = (int)Mathf.RoundToInt(((mPos.y / (Screen.width / 1024f)) - 140) / 86);
                    posX = (int)Mathf.Clamp(posX, 0, 14);
                    posY = (int)Mathf.Clamp(posY, -1, 0);
                    int index = posX + posY * -14;
                    int way = 0;
                    if(renderedTiles[index] != null)
                    {
                        for (int rndIndex = 1; rndIndex <= 14; rndIndex++)
                        {
                            if (posX + rndIndex < 14)
                            {
                                index = (posX + rndIndex) + posY * -14;
                                if (renderedTiles[index] == null)
                                {
                                    way = 1;
                                    break;
                                }
                            }
                            if (posX - rndIndex >= 0)
                            {
                                index = (posX - rndIndex) + posY * -14;
                                if (renderedTiles[index] == null)
                                {
                                    way = -1;
                                    break;
                                }
                            }
                        }
                    }
                    index = posX + posY * -14;
                    bool found = false;
                    while(found == false)
                    {
                        if (renderedTiles[index] == null)
                        {
                            renderedTiles[index] = playerTiles[clickedIndex];
                            found = true;
                        }
                        else
                        {
                            int cIndex = playerTiles.IndexOf(renderedTiles[index]);
                            renderedTiles[index] = playerTiles[clickedIndex];
                            clickedIndex = cIndex;
                        }
                        index += way;
                    }
                    for(int pt = 0; pt < playerTiles.Count; pt++)
                    {
                        int indexXY = renderedTiles.IndexOf(playerTiles[pt]);
                        int tilePositionX = indexXY % 14;
                        int tilePositionY = indexXY >= 14 ? -1 : 0;
                        targetPos[pt] = new Vector3((70 + tilePositionX * 68) * (Screen.width / 1024f), (140 + tilePositionY * 86) * (Screen.width / 1024f), 0);
                    }
                }
                else if (clickedIndex != -1 && Time.timeSinceLevelLoad - clickStartTime > 0.2f)
                {
                    Vector3 mPos = Input.mousePosition;
                    for (int i = 0; i < playerTiles.Count; i++)
                    {
                        if (Vector3.Distance(playerTiles[i].transform.position, mPos) < (32.0f * (Screen.width / 1024f)))
                        {
                            if(clickedIndex == i)
                            {
                                controller.Notify(NotifyCommand.Log, playerTiles[i].ToString());
                                if (orderedTiles[i].isWildTile)
                                {
                                    isWildTileTurnedFront = !isWildTileTurnedFront;

                                    if (!isWildTileTurnedFront)
                                    {
                                        playerTiles[i].GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/BACK", typeof(Texture));
                                    }
                                    else
                                    {
                                        playerTiles[i].GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/" + orderedTiles[i].ToString(), typeof(Texture));
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }


        public void SetTiles()
        {
            List<Vector3> oldPos = new List<Vector3>(targetPos);
            if(playerTiles.Count == 0)
            {
                oldPos.Add(mainCanvasController.WildTileElement.transform.position);
                moveDelayMultiplier = 1.0f;
            }
            else
            {
                moveDelayMultiplier = 0.0f;
            }
            playerTiles.Clear();
            orderedTiles.Clear();
            renderedTiles.Clear();
            for(int r = 0; r < 28; r++)
            {
                renderedTiles.Add(null);
            }
            targetPos.Clear();
            moveDelay.Clear();
            foreach(Transform tile in tileParent)
            {
                GameObject.Destroy(tile.gameObject);
            }
            mainCanvasController.WildTileElement.SetActive(Conf.configuration.gameConf.wildTileEnabled);
            if (Conf.configuration.gameConf.wildTileEnabled)
            {
                mainCanvasController.WildTileElement.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/" + GameData.GD.wildTileElement.ToString(), typeof(Texture));
            }

            int tilePositionX = 0;
            int tilePositionY = 0;
            if (GameData.GD.groups.Count == 0)
            {
                for (int tileNo = 0; tileNo < GameData.GD.playerTiles.Count; tileNo++)
                {
                    if (tilePositionX > 13) // 14 one line element count limit
                    {
                        tilePositionY--;
                        tilePositionX = 0;
                    }
                    TileElement tileElement = GameData.GD.playerTiles[tileNo];
                    GameObject tileObject = GameObject.Instantiate((GameObject)GetResource("Prefabs/TileElement", typeof(GameObject)), tileParent);
                    tileObject.transform.position = oldPos[tileElement.oldIndex];
                    tileElement.oldIndex = targetPos.Count;
                    tileObject.transform.SetSiblingIndex(0);
                    if (tileElement.isWildTile && !isWildTileTurnedFront)
                    {
                        tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/BACK", typeof(Texture));
                    }
                    else
                    {
                        tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/" + tileElement.ToString(), typeof(Texture));
                    }
                    tileObject.name = tileElement.ToString();
                    playerTiles.Add(tileObject);
                    orderedTiles.Add(tileElement);
                    targetPos.Add(new Vector3((70 + tilePositionX * 68) * (Screen.width / 1024f), (140 + tilePositionY * 86) * (Screen.width / 1024f), 0));
                    moveDelay.Add(moveDelay.Count * 0.1f * moveDelayMultiplier);
                    renderedTiles[tilePositionX + (tilePositionY * -14)] = tileObject;
                    tilePositionX++;
                }
            }
            else
            {
                for (int groupNo = 0; groupNo < GameData.GD.groups.Count; groupNo++)
                {
                    if (tilePositionX + GameData.GD.groups[groupNo].Count > 13) // 14 one line element count limit
                    {
                        tilePositionY--;
                        tilePositionX = 0;
                    }
                    for (int tileNo = 0; tileNo < GameData.GD.groups[groupNo].Count; tileNo++)
                    {
                        TileElement tileElement = GameData.GD.groups[groupNo][tileNo];
                        GameObject tileObject = GameObject.Instantiate((GameObject)GetResource("Prefabs/TileElement", typeof(GameObject)), tileParent);
                        tileObject.transform.position = oldPos[tileElement.oldIndex];
                        tileElement.oldIndex = targetPos.Count;
                        tileObject.transform.SetSiblingIndex(0);
                        if (tileElement.isWildTile && !isWildTileTurnedFront)
                        {
                            tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/BACK", typeof(Texture));
                        }
                        else
                        {
                            tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/" + tileElement.ToString(), typeof(Texture));
                        }
                        tileObject.name = tileElement.ToString();
                        playerTiles.Add(tileObject);
                        orderedTiles.Add(tileElement);
                        targetPos.Add(new Vector3((70 + tilePositionX * 68) * (Screen.width / 1024f), (140 + tilePositionY * 86) * (Screen.width / 1024f), 0));
                        moveDelay.Add(moveDelay.Count * 0.1f * moveDelayMultiplier);
                        renderedTiles[tilePositionX + (tilePositionY * -14)] = tileObject;
                        tilePositionX++;
                    }
                    tilePositionX++;
                }
                for (int tileNo = 0; tileNo < GameData.GD.availableTiles.Count; tileNo++)
                {
                    if (tilePositionX > 13) // 14 one line element count limit
                    {
                        tilePositionY--;
                        tilePositionX = 0;
                    }
                    TileElement tileElement = GameData.GD.availableTiles[tileNo];
                    GameObject tileObject = GameObject.Instantiate((GameObject)GetResource("Prefabs/TileElement", typeof(GameObject)), tileParent);
                    tileObject.transform.position = oldPos[tileElement.oldIndex];
                    tileElement.oldIndex = targetPos.Count;
                    tileObject.transform.SetSiblingIndex(0);
                    if (tileElement.isWildTile && !isWildTileTurnedFront)
                    {
                        tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/BACK", typeof(Texture));
                    }
                    else
                    {
                        tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/" + tileElement.ToString(), typeof(Texture));
                    }
                    tileObject.name = tileElement.ToString();
                    playerTiles.Add(tileObject);
                    orderedTiles.Add(tileElement);
                    targetPos.Add(new Vector3((70 + tilePositionX * 68) * (Screen.width / 1024f), (140 + tilePositionY * 86) * (Screen.width / 1024f), 0));
                    moveDelay.Add(moveDelay.Count * 0.1f * moveDelayMultiplier);
                    renderedTiles[tilePositionX + (tilePositionY * -14)] = tileObject;
                    tilePositionX++;
                }
                for (int tileNo = 0; tileNo < GameData.GD.unpairedTiles.Count; tileNo++)
                {
                    if (tilePositionX > 13) // 14 one line element count limit
                    {
                        tilePositionY--;
                        tilePositionX = 0;
                    }
                    TileElement tileElement = GameData.GD.unpairedTiles[tileNo];
                    GameObject tileObject = GameObject.Instantiate((GameObject)GetResource("Prefabs/TileElement", typeof(GameObject)), tileParent);
                    tileObject.transform.position = oldPos[tileElement.oldIndex];
                    tileElement.oldIndex = targetPos.Count;
                    tileObject.transform.SetSiblingIndex(0);
                    if (tileElement.isWildTile && !isWildTileTurnedFront)
                    {
                        tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/BACK", typeof(Texture));
                    }
                    else
                    {
                        tileObject.GetComponent<RawImage>().texture = (Texture)GetResource("Images/Sprites/" + tileElement.ToString(), typeof(Texture));
                    }
                    tileObject.name = tileElement.ToString();
                    playerTiles.Add(tileObject);
                    orderedTiles.Add(tileElement);
                    targetPos.Add(new Vector3((70 + tilePositionX * 68) * (Screen.width / 1024f), (140 + tilePositionY * 86) * (Screen.width / 1024f), 0));
                    moveDelay.Add(moveDelay.Count * 0.1f * moveDelayMultiplier);
                    renderedTiles[tilePositionX + (tilePositionY * -14)] = tileObject;
                    tilePositionX++;
                }
            }
            oldPos = targetPos;
        }

        private Object GetResource(string key, System.Type type)
        {
            if (!resources.ContainsKey(key))
            {
                resources.Add(key, Resources.Load(key, type));
            }
            return resources[key];
        }

        private void ColorsSort()
        {
            controller.Notify(NotifyCommand.ColorSort);
        }

        private void NumbersSort()
        {
            controller.Notify(NotifyCommand.NumberSort);
        }

        private void AutoSort()
        {
            controller.Notify(NotifyCommand.AutoSort);
        }

        private void Reload()
        {
            controller.Notify(NotifyCommand.Reload);
        }


        public void Notify(NotifyCommand command, object data = null)
        {
            switch(command)
            {
                case NotifyCommand.RemainingCountChanged:
                    {
                        mainCanvasController.RemainingCount.text = GameData.GD.remainingTileCount.ToString();
                        break;
                    }
            }
        }
    }
}
